package billobob.org.speechtest;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Simple app for recognizing speech
 */
public class MainActivityFragment extends Fragment {

    protected static final int RESULT_SPEECH = 1234;

    private TextView mSpeechTextView1;
    private TextView mSpeechTextView2;
    private Button mSpeechButton;
    private String speechString;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    boolean mIsListening = false;
    TextToSpeech t1;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mSpeechTextView1 = (TextView) view.findViewById(R.id.textView1);
        mSpeechTextView2 = (TextView) view.findViewById(R.id.textView2);
        mSpeechButton = (Button) view.findViewById(R.id.speechButton);
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.getContext());
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getActivity().getPackageName());

         t1 = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });


        SpeechRecognitionListener listener = new SpeechRecognitionListener();
        mSpeechRecognizer.setRecognitionListener(listener);

        mSpeechButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsListening)
                {
                    mIsListening = true;
                    mSpeechTextView1.setText("Item 1 (Location)");
                    mSpeechTextView2.setText("Item 2 (place)");
                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                }
            }
        });
        return view;
    }

    @Override

    public void onDestroyView() {
        if (mSpeechRecognizer != null)
        {
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
        }
        super.onDestroyView();
    }

    protected class SpeechRecognitionListener implements RecognitionListener {
        private static final java.lang.String DELIMITER = "in ";

        @Override
        public void onReadyForSpeech(Bundle params) {
            Log.d("UUSP", "in read");
        }

        @Override
        public void onBeginningOfSpeech() {
            Log.d("UUSP", "begin!");
        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {
            Log.d("UUSP", "end");
            // We set listening to false here in case no "results" are returned
            mIsListening = false;
        }

        @Override
        public void onError(int error) {

        }

        @Override
        public void onResults(Bundle results) {
            // TODO: add a message in case results == null
            // TODO: SpeechRecognizer has "confidence values"--mention it/give example?
            ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            Log.d("UUSP", matches != null ? matches.get(0) : null);

            String text = matches.get(0);

            if (text.contains(DELIMITER)) {

                String[] speeched = text.split(DELIMITER);

                mSpeechTextView1.setText(speeched[0]);
                mSpeechTextView2.setText(speeched[1]);
            }

            else {
                if (mSpeechTextView1.getText().equals("Item 1 (Location)")){
                    mSpeechTextView1.setText(text);

                    t1.speak("Where is this damn place?", TextToSpeech.QUEUE_FLUSH, null);

                    while (t1.isSpeaking()) {
                        continue;
                    }

                    mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                }
                else {
                    mSpeechTextView2.setText(text);
                }
            }


        }

        @Override
        public void onPartialResults(Bundle partialResults) {
            Log.d("UUSP", "partial...");
        }

        @Override
        public void onEvent(int eventType, Bundle params) {
            Log.d("UUSP", "event?");
        }
    }
}
